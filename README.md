# Collected lecture notes in computational biology

Here are the documents from the tutorials I am in charge of as a
TA. I give them here without any guarantee of correctness. I mostly
wrote them as notes for myself. I hope you will find them useful. Do
not hesitate to send me a message or a pull request if you find typo
and errors. You can redistribute them under a creative commons
CC-BY-SA license.

The most basic lectures (Python and Mathematics) are targeted to
undergrad and are in French, whereas the more advanced ones (Modeling
and Adaptive dynamics) are in English.
