#+title: Adaptive Dynamics (2) -- Tutorial
#+author: Guilhem Doulcier
#+options: toc:nil
#+latex_header: \usepackage{a4wide}
#+latex_header: \usepackage{listings}
#+latex_header: \usepackage{color}
#+latex_header: \definecolor{mymauve}{rgb}{0.58,0,0.82}
#+latex_header: \lstset{commentstyle=\color{mymauve},frame=single,basicstyle=\footnotesize,texcl=true,extendedchars=true,inputencoding=latin1}

This tutorial follows the first tutorial on Pairwise Invasibility
Plots. We will use the same model (from Claessen 2007). The objective
is to present stochastic simulations that allow to relax some
assumptions of adaptive dynamics and check its predictions.

*Concepts*: Stochastic simulations,  Zen software.

* Biological problem

Let us come back to the problem presented in DYAD01.

The adaptive dynamics framework allow us to make prediction about the
evolutionary trajectory of our system: i.e. the trajectory of the
heritable trait $u$ in a resident population at equilibrium, as it is
invaded by successive (initially rare) mutants.

Simulations are a way to relax some modeling assumptions and see if
they still hold up when we introduce more complexity.

* Modeling the problem

We will use the hypothesis introduced in the main lecture:
- Haploid asexual populations,
- Each individual characterized by a trait $u \in [0,1]$.
- Each individual have a birth rate $b$ and a death rate $d$.
- Each time an individual reproduce, it spawns a individual with the
  same trait $u$ than its parent with probability $(1-\mu)$ or a
  mutated trait $u+\epsilon(u)$ with probability $\mu$. $\epsilon(u)$
  is a normal random variable centered on $u$ and clamped so
  $u+\epsilon(u) \in [0,1]$ a.s.

So we have a set of individuals that give birth and die. Let us
imagine that we have one individual, $d=0$. What kind of random
variable will we use to model the time before which we have two individuals ?

The answer is of course the *Exponential distribution*.

\begin{equation*}
X \sim \mathcal {E} \text{xp}(\lambda)
\Rightarrow
\begin{cases}
\mathbb P(X \in A  ) = \int_A \lambda e^{-\lambda x} dx\\
\mathbb E(x) = \frac{1}{\lambda}
\end{cases}
\end{equation*}

Could you cite the two most important properties of the exponential distribution ?

- Memoryless :: $\mathbb P(X>s+t | X>s) = \mathbb P(X>t)$
- Law of the minimum of independent exponential variables ::
$X_i \sim \mathcal Exp(\lambda_i)$, $Y_i=\min_i X_i$, $Z_i = \text{argmin}_i X_i$  $\Rightarrow Y \sim \mathcal Exp(\sum_i \lambda i); \; \mathbb P(Z=i) = \frac{\lambda_i}{\sum_j \lambda_j}$

These are quite easy to prove from the definition of the exponential
distribution. If you have never done the proof, give it a try.

* Simulations

** Exact stochastic simulation
Let us consider a first way to simulate our process, the naive
Doob-Gillespie Algorithm (You may have seen it in Math02). The core
idea of the algorithm is to always compute which is the next event to
occur, apply it and rinse and repeat.

Here is the pseudo-code:

#+begin_src python
#Initialize a data structure to store all individuals and their traits.
t = 0
while t<T:
    #Compute all the birth/death rates.
    #Sample from the exponential distribution the time of the next event dt.
    t += dt
    #Sample from a multinomial distribution the identity of the next event.
    #if it it a birth event, manage the eventual mutation
    #Update the list of individuals
#+end_src

Notice how the two properties of Exponential variable come into play !

The problem with this algorithm is that it is really slow for large
population sizes. You need to sample two random number at each event
and recompute all the rates. We have to find a better way.

** Reducing the state space.

You would have noticed that I was staying quite vague about the data
structure that holds the population. Of course, having a long list
with all the individuals is not efficient. It is much better to have a
small state space.

Let us come back on the assumption of the adaptive dynamics: the
mutations are rare enough so that the population is essentially
dimorphic: a resident and mutant at all time. We can take $\mathbb N^2_+$ as the
state space. So at all point in time we just have to store the value
of the resident and mutant trait and the number of individuals.

** Discrete time

Instead of simulating every single event, we could "integrate" what
happen over a small time window. The techniques presented here
relies on the same assumption: we suppose that for a small integration
time $\Delta t$, the birth-death process have constant birth and
death rates $b$ and $d$. We then recompute

#+BEGIN_SRC python
   t = 0
   while t < T:
     # increase the time
     t += dt
     # Compute the new birth/death rates.
     b,d = get_birth_death_rate(n)
     # Compute the new population size after dt based on the constant rates.
     n += birth_death_discrete(n,b,d,dt)
#+END_SRC

- Poisson approximation ::

A first method that is not very computationally intensive.

#+BEGIN_SRC python
def birth_death_discrete(n,b,d,dt):
    return Poisson(n*exp((b-d)*dt))
#+END_SRC

This method is presented as "non rigorous but giving satisfactory
results" in the reference" in the Zen manual \ldots

Notice that on average, we get $ne^{(b-d)dt}$ individuals, which is
coherent with the $\frac{dN}{dt} = N (b-d)$ differential equation with
initial condition $N(0)=n$

Reference of this method can be found in the ZEN reference manual
section "Evolutionary dynamics" when the implementation of
=kisdi.zen= is presented.


** Using python

If you want a small example implementation of this process in
python. Look at the notebook distributed along those notes. Example of
output with mutant and resident:

[[file:trajectory.png]]

* Where does it stop ? More about branching

We saw in the Kisdi example that the branches where not going to the
max value of the phenotype. It seems to be the case in the Claessen
model.

Can adaptive dynamics tell us more than just: there might be a
branching here ? Yes ! using a TEP (trait evolution plot).

You remember how the PIP is interested in how a rare mutant can invade
a resident population ? We considered that if the rare mutant per
capita growth rate was positive ($s_r(m)$), the mutant would
invade. We completely disregard the rest of the invasion dynamics.

There is something easy we can look at though: the end of the invasion
dynamics. If a mutant somehow managed to be in high enough proportion
so that it is the one at equilibrium, and the resident is in rare
proportion, is the mutant able to drive the resident to extinction ?


[[file:tep.pdf]]


The answer of course is to look at $s_m(r)$. This can be done by just
flipping the PIP along the diagonal (red in the figure).

Now if we superimpose the PIP with its flipped version, we get 3 kinds
of zone:
- The mutant increases if rare, the resident decreases if rare (blue).
- The mutant decreases if rare, the resident increases if rare (red).
- Both mutant and resident increase if rare (green).

This last situation is called a *protected polymorphism*. In this
region we predict that the mutant can invade but not exclude the
resident leading to a dimorphic population.

One may add the isoclines zero of the polymorphic invasion fitness
(cf. main lecture) to find the final position of the branches.

* Redacted part                                                    :noexport:
((*NOTE TO SELF* In 2017, I stopped here the presentation of the methods, show the python results and skipped directly to the ZEN program.))

- Binomial approximation ::

We look at a small time $\Delta t$, what is the probability that an
individual with birth rate $b$ and death rate $d$ will die or
reproduce during $\Delta t$ ? Answer: It is a random variable $T \sim
\mathcal Exp(b+d)$

\begin{align*}
P(T < \Delta t) &= \int_0^\Delta t (b+d) e^{-(b+d) x} dx \\
&= 1 - e^{(b+d) \Delta t}
\end{align*}

Thus:

#+BEGIN_SRC python
def birth_death_discrete(n,b,d,dt):
    Get k the number of birth or death event which follow Binomial(n, 1-exp(b-d * dt))
    Get m the number of death event, which follow Binomial(k, d/(b+d))
    Return n - m + (k-m)
#+END_SRC

This approximation only works for relatively small $\Delta t$, because
we suppose that the probability that two succecive events occur during
$\Delta t$ negligible.


- Solving the Kolmogorov-Forward Equation ::

This leads to the most rigorous approximation, more robust to big
$\Delta t$, but it is also more computationally expensive. In fact,
this computation is exact if the birth and death rate are
constant. Reference for this method can be found in the ZEN reference
manual section "From continuous to discrete time".

Let $P_n(t) = \mathbb P(Z_{t} = n)$ be the probability that there is
$n$ individuals at time $t$. If we look at the first jump, we can have
a recursive expression:

((*Note to self: The following might be wrong*))

\begin{align*}
P_n(t+h) &= \overbrace{P_n(t)(1 - b h - d h)}^{\text{no jump in} [t,t+h]} + \overbrace{P_{n-1}(t)(bh)}^{\text{One birth in } [t,t+h]} + \overbrace{P_{n+1}(t)(dh)}^{\text{One death in } [t,t+h]} + o(h)\\
\frac{P_n(t+h) - P_n(t)}{h} &= - P_n(t)(b + d) + bP_{n-1}(t)(bh) + dP_{n-1}(t)\\
P'_n(t) &= - P_n(t)(b + d) + bP_{n-1}(t) + dP_{n-1}(t)\\
\end{align*}

This system of differential equation are called the Kolomogorov
Forward Equation of our birth-death process. Solving them with initial
condition $Z_0 = 1$ leads to:
\begin{equation*}
\mathbb P(Z_t = k) = (1 - \rho)^k \rho
\end{equation*}

\begin{align*}
b<d&: \; \mathbb P(Z_t = 0) = \frac{d(1-e^{(b-d)\Delta t})}{d - be^{(b-d )\Delta t}}, \rho = \frac{d-b}{d-be^{(b-d)\Delta t}}\\
b=d&: \; \mathbb P(Z_t = 0) = \frac{b\Delta t}{1+b\Delta}, \rho = \frac{1}{1+b\Delta}\\
b>d&: \; \mathbb P(Z_t = 0) = \frac{d(1-e^{(b-d)\Delta t})}{d - be^{-(b-d )\Delta t}}, \rho = \frac{(b-d)e^{-(b-d)\Delta t}}{b-de^{-(b-d)\Delta t}}
\end{align*}

Which leads us to:

#+BEGIN_SRC python
def birth_death_discrete(n,b,d,dt):
   Find the number of lineages do not go extinct before dt (binomial (n,1-p0))
   Sum the size of all the non extinct lineages after dt (sum of geometric(rho)).
   Return it.
#+END_SRC

** Using Zen

Zen is a software developed by Stéphane Legendre in IBENS
(eco-evo-math team) designed to study population dynamics models, with
an evolutionary component in the line of the theory of adaptive
dynamics.

Get it there:
- https://www.biologie.ens.fr/~legendre/zen/zen.html (stable)
- https://gitlab.com/ecoevomath/zen (development)


The structure of a zen model:

/Here the T.A explain how a zen model is structure while drawing the
schematic below on the whiteboard/.

[[file:kisdi_structure.pdf]]

Zen has four important kinds of objects: model, group, relations and mutations:

- Model :: The master object, frame everything.
- Group :: An adaptive trait, its mutation and the related dynamics
- Relation :: A difference equation that is evaluated at each
	      time step. There must be at least one per group and
	      model.
- Mutation :: Store the characteristics of the mutation, when it
	      happen, the distribution from which mutants traits are
	      drawn, which variable are concerned.

Let us walk through a model implemented in zen: =kisdi.zen= from (Kisdi
E & S Geritz. 2001).

\begin{equation*}
\frac{dN_i}{dt} = n_i r(s_i) - \sum_j \alpha(s_i,s_j) n_j)
\end{equation*}

with

\begin{equation*}
\begin{cases}
r(s) = 4 - s \\
\alpha(s_i,s_j) = c \left (1 - \frac{1}{1+\nu e^{-k(s_1-s_2)}} \right) + a
\end{cases}
\end{equation*}

Open =kisdi.zen=  and execute this:

#+BEGIN_SRC pascal
{ display total population size
{         graph t ntot
{         run 100

{ display first branching
{         init
{         graph s t
{         xscale 0 4
{         run 4000 1000

{ display full figure
{         init
{         run 30000
#+END_SRC

*Exercise*: Look at the =kisdi.zen= file bundled with Zen and modify it to
implement our model.

Correction can be found in =claessen.zen=.


[[file:claessen_using_zen.jpg]]
