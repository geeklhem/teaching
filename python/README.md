# Un second cours de Python

(L3 level ~ In french) These notes are meant to be the backbone of a
"second course" in Python programming. They assume that the students
already have basic knowledge of programming in general and python in
particular. They are really a collection of interesting concepts and
pitfalls that most scientist will come across in their day to day
job.

I used them during the "Advanced Python Programming" lectures taught
by Dr. Pierre Vincens in 2018 in the Biology department of the École
normale supérieure de Paris.
